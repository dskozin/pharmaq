<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('description', 500)->default("");
            $table->integer('product_type_id');
            $table->integer('mnn_id')->nullable();
            $table->boolean('mnn_used')->default(false);
            $table->string('formula', 160)->nullable();
            $table->string('img_src')->nullable();
            $table->string('imp_src')->nullable();
            $table->integer('primary_package_id')->nullable();
            $table->integer('secondary_package_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
