<?php

use Crockett\CsvSeeder\CsvSeeder;

class SecondaryPackagingCSVSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->filename = base_path('database/seeds/csv/second_pack.csv');
        $this->delimiter = ';';
        $this->table = \App\Models\Dictionaries\SecondaryPackage::class;
        $this->mapping = [
            0 => 'code',
            1 => 'name',
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::run();
    }
}
