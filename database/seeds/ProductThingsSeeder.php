<?php

use Illuminate\Database\Seeder;

class ProductThingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductTypeCSVSeeder::class);
        $this->call(PrimaryPackagingCSVSeeder::class);
        $this->call(SecondaryPackagingCSVSeeder::class);
    }
}
