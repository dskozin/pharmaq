<?php

use App\Models\Auth\Role;
use App\Models\Auth\Permission;
use Illuminate\Database\Seeder;

class ConnectRelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    $systemAdmitPermissionSlugs = [
            'view.system_info',
            'edit.system_info',
            'view.users',
            'create.users',
            'edit.users',
            'delete.users'
        ];
		$systemAdminPermissions = Permission::whereIn('slug', $systemAdmitPermissionSlugs);
		$roleSystemAdmin = Role::where('slug', 'system_admin')->first();
		foreach ($systemAdminPermissions as $permission){
		    $roleSystemAdmin->attachPermission($permission);
        }

        $adminPermissionSlugs = [
            'view.users',
            'create.users',
            'edit.users',
            'delete.users'
        ];
		$adminPermissions = Permission::whereIn('slug', $adminPermissionSlugs);
		$roleAdmin = Role::where('slug', 'admin')->first();
		foreach ($adminPermissions as $permission) {
			$roleAdmin->attachPermission($permission);
		}

    }

}