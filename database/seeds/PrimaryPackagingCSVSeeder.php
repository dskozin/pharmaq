<?php

use Crockett\CsvSeeder\CsvSeeder;

class PrimaryPackagingCSVSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->filename = base_path('database/seeds/csv/primary_pack.csv');
        $this->delimiter = ';';
        $this->model = \App\Models\Dictionaries\PrimaryPackage::class;
        $this->mapping = [
            0 => 'code',
            1 => 'name',
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::run();
    }
}
