<?php

use Illuminate\Database\Seeder;

class CommonDatabaseSeeder extends Seeder
{
    public function run()
    {
        //создаем базовые права для создания/редактирования пользователей
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);

        //создаем в базе типы препаратов и упаковок
        $this->call(ProductThingsSeeder::class);
    }
}