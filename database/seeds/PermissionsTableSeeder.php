<?php

use App\Models\Auth\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    /**
	     * Add Permissions
	     *
	     */
        if (Permission::where('name', '=', 'Can View Users')->first() === null) {
			Permission::create([
			    'name' => 'Can View Users',
			    'slug' => 'view.users',
			    'description' => 'Can view users',
			    'model' => 'Permission',
			]);
        }

        if (Permission::where('name', '=', 'Can Create Users')->first() === null) {
			Permission::create([
			    'name' => 'Can Create Users',
			    'slug' => 'create.users',
			    'description' => 'Can create new users',
			    'model' => 'Permission',
			]);
        }

        if (Permission::where('name', '=', 'Can Edit Users')->first() === null) {
			Permission::create([
			    'name' => 'Can Edit Users',
			    'slug' => 'edit.users',
			    'description' => 'Can edit users',
			    'model' => 'Permission',
			]);
        }

        if (Permission::where('name', '=', 'Can Delete Users')->first() === null) {
			Permission::create([
			    'name' => 'Can Delete Users',
			    'slug' => 'delete.users',
			    'description' => 'Can delete users',
			    'model' => 'Permission',
			]);
        }

        /**
         * Системные разрешения
         *
         */
        if (Permission::where('slug', '=', 'view.system_info')->first() === null) {
            Permission::create([
                'name' => 'Может смотреть системную информацию',
                'slug' => 'view.system_info',
                'description' => 'Может просматривать системную информацию',
                'model' => 'Permission',
            ]);
        }

        if (Permission::where('slug', '=', 'edit.system_info')->first() === null) {
            Permission::create([
                'name' => 'Может редактировать системную информацию',
                'slug' => 'edit.system_info',
                'description' => 'Может редактировать системную информацию',
                'model' => 'Permission',
            ]);
        }

    }
}
