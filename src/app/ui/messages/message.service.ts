import {Injectable} from '@angular/core';
import {Message} from "./message";
import {MessageType} from "./message-type";


@Injectable()
export class MessageService {

    constructor() {
    }

    messages: Message[] = [];

    add(message: string, type: MessageType) {
        this.messages.push(new Message(message, type));
    }

    clear(): void {
        this.messages = [];
    }

    delete(message: Message) {
        const index: number = this.messages.indexOf(message);
        if (index != -1) {
            this.messages.splice(index, 1);
        }
    }
}
