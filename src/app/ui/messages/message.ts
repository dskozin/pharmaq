import {MessageType} from "./message-type";

export class Message{
    message: string;
    type: MessageType;

    constructor(message: string, type: MessageType){
        this.message = message;
        this.type = type;
    }
}

