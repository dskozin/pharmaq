export enum MessageType {
    Info = "alert-info",
    Danger = "alert-danger",
}