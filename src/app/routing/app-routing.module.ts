import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ProductListComponent} from "../product/product-list/product-list.component";
import {DashboardComponent} from "../ui/dashboard/dashboard.component";
import {ProductDetailComponent} from "../product/product-detail/product-detail.component";
import {ProductFormComponent} from "../product/product-form/product-form.component";

const routes: Routes = [
    {path: 'dashboard', component: DashboardComponent},
    {path: '', redirectTo: '/dashboard', pathMatch: 'full'},

    {path: 'products', component: ProductListComponent},
    {path: 'products/create', component: ProductFormComponent},
    {path: 'products/:id', component: ProductDetailComponent},
];


@NgModule({
    exports: [
        RouterModule
    ],

    imports: [
        RouterModule.forRoot(routes)
    ],
})
export class AppRoutingModule {
}