import { Component, OnInit } from '@angular/core';
import {Product} from "../../product";

@Component({
  selector: 'app-product-row',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.css']
})
export class ProductRowComponent implements OnInit {

  product:Product;

  constructor() { }

  ngOnInit() {
  }

}
