export class Product {
    id: number;
    name: string;
    output_form: string;
    product_type_id: number;
    mnn_id: number;
    mnn_used: boolean;
    formula: string;
    img_src: string;
    imp_src: string;
    primary_package_id: number;
    secondary_package_id: number;
}