import {Component, OnInit, ViewChild} from '@angular/core';
import {Product} from "../product";
import {ProductService} from "../product.service";
import {NgModel} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
    selector: 'app-product-form',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

    product: Product = new Product();
    errors = [];

    constructor(private productService: ProductService,
                private router: Router) {
        this.product.mnn_used = true;
    }

    ngOnInit() {
    }

    onSubmit() {
        this.errors = [];
        this.productService.postProduct(this.product).subscribe(
            response => this.product = response['product'],
            response => this.errors = response.error.errors
        )
    }

    hasErrors(field: string): boolean {
        return this.errors[field] != undefined;
    }

    getErrorFor(field: string): string {
        if (this.errors[field] != undefined)
            return this.errors[field][0];

        return '';
    }

    onCancel() {


        //проверка что форма чистая, если нет, то необходимо подтверждение
        this.router.navigateByUrl("/products");
    }
}
