import {ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit} from '@angular/core';
import {DictionaryEntry} from "./dictionary";
import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {DictionaryService} from "./dictionary-service.service";

@Component({
    selector: 'app-dictionary',
    templateUrl: './dictionary.component.html',
    styleUrls: ['./dictionary.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DictionaryComponent),
            multi: true
        }
    ]
})
export class DictionaryComponent implements OnInit, ControlValueAccessor {

    dictionaryEntries: DictionaryEntry[] = [];
    dictionaryEntriesLoading: boolean = false;
    dictionaryEntryTypeAhead = new EventEmitter<string>();
    dictionarySearching = false;
    @Input() usedDictionary: string;
    @Input() hasError: boolean = false;
    @Input() isEnabled: boolean = true;
    @Input() needToScroll: boolean = true;

    propagateChange = (_: any) => {};

    _selectedDictionaryEntry: number;

    constructor(private dictionaryService: DictionaryService,
                private cd: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.dictionaryService
            .getDictionaryEntries(this.usedDictionary, 0)
            .subscribe(
                dictionaryEntries => this.dictionaryEntries = dictionaryEntries
            );
        this.serverSideSearch();
    }

    private serverSideSearch() {
        this.dictionaryEntryTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(200),
            switchMap(findString => {
                this.dictionarySearching = true;
                return this.dictionaryService
                    .getDictionaryEntries(this.usedDictionary,0, findString)
            })
        ).subscribe(x => {
            this.cd.markForCheck();
            this.dictionaryEntries = x;
        }, (err) => {
            console.log(err);
            this.dictionaryEntries = [];
        });

    }

    fetchMoreDictionaryEntries() {
        if (this.dictionarySearching || !this.needToScroll)
            return;

        const len = this.dictionaryEntries.length;
        this.dictionaryEntriesLoading = true;
        this.dictionaryService.getDictionaryEntries(this.usedDictionary,len).subscribe(
            newDictionaryEntries => {
                this.dictionaryEntries = this.dictionaryEntries.concat(newDictionaryEntries);
                this.dictionaryEntriesLoading = false;
            }
        );
    }

    resetProductTypesField() {
        if (this.dictionarySearching) {
            this.dictionaryEntries = [];
            this.dictionarySearching = false;
            this.fetchMoreDictionaryEntries();
        }
    }

    get selectedDictionaryEntry() {
        return this._selectedDictionaryEntry;
    }

    set selectedDictionaryEntry(val) {
        this._selectedDictionaryEntry = val;
        this.propagateChange(this._selectedDictionaryEntry);
    }

    writeValue(obj: any): void {
        if (obj !== undefined) {
            this.selectedDictionaryEntry = obj;
        }
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
