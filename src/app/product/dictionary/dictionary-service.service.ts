import {Injectable} from '@angular/core';
import {MessageType} from "../../ui/messages/message-type";
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import {DictionaryEntry} from "./dictionary";
import {MessageService} from "../../ui/messages/message.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class DictionaryService {


    private dictionaryURLs = {
        productTypes: 'http://localhost:8000/api/products/product_types',
        primaryPackages: 'http://localhost:8000/api/products/primary_packages',
        secondaryPackages: 'http://localhost:8000/api/products/secondary_packages',
    };

    constructor(private messageService: MessageService,
                private httpClient: HttpClient) {
    }

    getDictionaryEntries(dictionary: string, offset?: number, findString?: string): Observable<DictionaryEntry[]> {
        let param = offset > 0 ? '/' + offset : '/0';
        param = param.concat(findString ? '/' + findString : '');

        return this.httpClient.get<DictionaryEntry[]>(this.dictionaryURLs[dictionary].concat(param))
            .pipe(
                catchError(
                    this.handleError('getDictionaryEntries', [])
                )
            );
    }

    private handleError<T>(operation: string, result?: T) {
        return (error: any): Observable<T> => {

            this.messageService.add(`Операция ${operation} не исполнена по причине: ${error.message}`, MessageType.Danger);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
