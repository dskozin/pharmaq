export interface DictionaryEntry{
    id: number;
    code: string;
    name: string;
}

export class ProductType implements DictionaryEntry{
    id: number;
    code: string;
    name: string;
}

export class MNN implements DictionaryEntry{
    id: number;
    code: string;
    name: string;
}

export class Package implements DictionaryEntry{
    id: number;
    code: string;
    name: string;
}