import {Injectable} from '@angular/core';
import {Product} from "./product";
import {Observable} from "rxjs/Observable";
import {MessageService} from "../ui/messages/message.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {catchError, tap} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import {MessageType} from "../ui/messages/message-type";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ProductService {

    private productUrl = 'http://localhost:8000/api/products';


    constructor(private messageService: MessageService,
                private httpClient: HttpClient) {
    }

    getProducts(): Observable<Product[]> {
        return this.httpClient.get<Product[]>(this.productUrl)
            .pipe(
                catchError(
                    this.handleError('getProduct', [])
                )
            );
    }

    getProduct(id: number): Observable<Product> {
        return this.httpClient.get<Product>(this.productUrl + '/' + id)
            .pipe(
                catchError(
                    this.handleError('getProducts', new Product())
                )
            );
    }

    postProduct(product: Product): Observable<HttpResponse<any>> {
        return this.httpClient.post<any>(this.productUrl, product, httpOptions)
            .pipe(
                tap(
                    data => data,
                    error => {
                        if (error.status === 422)
                            return error;

                        this.handleError('postProduct', product);
                    }
                )
            );
    }

    private handleError<T>(operation: string, result?: T) {
        return (error: any): Observable<T> => {
            //
            // //если это ошибка в форме пробрасываем в форму
            // if(error.status === 422)
            //     return error;

            this.messageService.add(`Операция ${operation} не исполнена по причине: ${error.message}`, MessageType.Danger);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}
