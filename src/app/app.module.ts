import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

import {AppComponent} from './app.component';

import {NavbarComponent} from './ui/navbar/navbar.component';
import {NgProgressModule} from '@ngx-progressbar/core';
import {NgProgressHttpModule} from '@ngx-progressbar/http';
import {SidebarComponent} from './ui/sidebar/sidebar.component';
import {NgSelectModule} from "@ng-select/ng-select";

//------------ Products ------------ //
import {ProductListComponent} from "./product/product-list/product-list.component";
import {ProductDetailComponent} from './product/product-detail/product-detail.component';
import {ProductService} from "./product/product.service";
import {ProductFormComponent} from './product/product-form/product-form.component';
import {DictionaryComponent} from './product/dictionary/dictionary.component';
import {DictionaryService} from "./product/dictionary/dictionary-service.service";

import {MessagesComponent} from './ui/messages/messages.component';
import {MessageService} from './ui/messages/message.service';

import {AppRoutingModule} from './routing/app-routing.module';

import {DashboardComponent} from './ui/dashboard/dashboard.component';


@NgModule({
    declarations: [
        AppComponent,
        ProductListComponent,
        ProductDetailComponent,
        MessagesComponent,
        NavbarComponent,
        SidebarComponent,
        ProductFormComponent,
        DashboardComponent,
        DictionaryComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        NgProgressModule.forRoot(),
        NgProgressHttpModule,
        NgSelectModule
    ],
    providers: [
        ProductService,
        MessageService,
        DictionaryService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
