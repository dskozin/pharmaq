<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('products')->group(function (){
    Route::get('product_types/{offset?}/{find_string?}', 'ProductController@productTypeSelector')->name('product.product_types');
    Route::get('primary_packages/{offset?}/{find_string?}', 'ProductController@primaryPackageSelector')->name('product.primary_packages');
    Route::get('secondary_packages/{offset?}/{find_string?}', 'ProductController@secondaryPackageSelector')->name('product.secondary_packages');
});

Route::resources([
   'products'       => 'ProductController'
]);
