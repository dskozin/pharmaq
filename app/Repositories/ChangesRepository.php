<?php

namespace App\Repositories;

use App\Models\Changes\Change;


class ChangesRepository
{

    public function getAllPaged(int $perPage = 20)
    {
        return Change::paginate($perPage);
    }

}