<?php

namespace App\Repositories;

use App\Models\Dictionaries\Batch;


class BatchesRepository
{

    public function getAllPaged(int $perPage = 20)
    {
        return Batch::paginate($perPage);
    }

}