<?php

namespace App\Repositories;

use App\Models\Dictionaries\MNN;
use App\Models\Dictionaries\PrimaryPackage;
use App\Models\Dictionaries\Product;
use App\Models\Dictionaries\ProductType;
use App\Models\Dictionaries\SecondaryPackage;


class ProductsRepository
{

    public function getAllPaged(int $perPage = 20)
    {
        return Product::paginate($perPage);
    }

    public function getById(int $id){
        return ProductType::find($id);
    }

    public function getProductTypesChunk(int $offset = 0){
        return ProductType::offset($offset)
            ->limit(50)
            ->get();
    }

    public function getProductTypesChunkFindByName(int $offset, string $findString){
        $findString = mb_strtolower($findString);
        return ProductType::where('name', 'like', "%{$findString}%")->get();
    }

    public function getMNNChunk(int $offset = 0){
        return MNN::offset($offset)
            ->limit(50)
            ->get();
    }

    public function getMNNChunkFindByName(int $offset, string $findString){
        $findString = mb_strtolower($findString);
        return MNN::where('name', 'like', "%{$findString}%")->get();
    }

    public function getPrimaryPackagesChunk(int $offset = 0){
        return PrimaryPackage::offset($offset)
            ->limit(50)
            ->get();
    }

    public function getPrimaryPackagesChunkFindByName(int $offset = 0, string $findString = ""){
        return PrimaryPackage::where('name', 'like', "%{$findString}%")->get();
    }

    public function getSecondaryPackagesChunk(int $offset = 0){
        return SecondaryPackage::offset($offset)
            ->limit(50)
            ->get();
    }

    public function getSecondaryPackagesChunkFindByName(int $offset = 0, string $findString = ""){
        return SecondaryPackage::where('name', 'like', "%{$findString}%")->get();
    }
}