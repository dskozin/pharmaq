<?php

namespace App\Models\Changes;

use Illuminate\Database\Eloquent\Model;

class Change extends Model
{

    protected $fillable = [
      'name',
      'description',
      'registration_date',
      'planned'
    ];


}
