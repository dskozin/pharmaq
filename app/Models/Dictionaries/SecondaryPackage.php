<?php

namespace App\Models\Dictionaries;

use Illuminate\Database\Eloquent\Model;

class SecondaryPackage extends Model
{
    protected $table = 'secondary_packages';
}
