<?php

namespace App\Models\Dictionaries;

use Illuminate\Database\Eloquent\Model;

class PrimaryPackage extends Model
{
    protected $table = 'primary_packages';
}
