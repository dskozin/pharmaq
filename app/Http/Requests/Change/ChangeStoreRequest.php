<?php

namespace App\Http\Requests\Change;

use Illuminate\Foundation\Http\FormRequest;

class ChangeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //пользователь может создать здесь форму?
        //return auth()->user()->has;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            //'description' => 'required',
            'registration_date' => 'required',
        ];
    }
}
