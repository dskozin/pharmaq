<?php

namespace App\Http\Controllers;

use App\Http\Requests\Change\ChangeStoreRequest;
use App\Models\Changes\Change;
use App\Repositories\ChangesRepository;
use Illuminate\Http\Request;

class ChangeController extends Controller
{

    private $repository;

    public function __construct(ChangesRepository $changesRepository)
    {
        $this->repository = $changesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('change.list', ['changes' => $this->repository->getAllPaged()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('change.form');
    }

    /**
     * Сохраняем новое изменение
     *
     * @param ChangeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChangeStoreRequest $request)
    {
        $change = Change::create([
            'name'                  =>  $request->name,
            'description'           =>  $request->description,
            'registration_date'     =>  $request->registration_date,
            'planned'               =>  $request->planned == 'on',
        ]);

        return view('change.show', ['change' => $change]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Changes\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function show(Change $change)
    {
        return view('change.show', ['change' => $change]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Changes\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function edit(Change $change)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Changes\Change $change
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Change $change)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Changes\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function destroy(Change $change)
    {
        //
    }
}
