<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductCreateRequest;
use App\Models\Dictionaries\Product;
use App\Repositories\ProductsRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\DataCollector\AjaxDataCollector;

class ProductController extends AjaxDataCollector
{

    private $repository;

    public function __construct(ProductsRepository $productsRepository)
    {
        $this->repository = $productsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->getAllPaged();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreateRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dictionaries\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dictionaries\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dictionaries\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dictionaries\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function productTypeSelector(int $offset = 0, string $find_string = ""){
        if (!$find_string)
            return $this->repository->getProductTypesChunk($offset);

        return $this->repository->getProductTypesChunkFindByName($offset, $find_string);
    }

    public function primaryPackageSelector(int $offset = 0, string $find_string = ""){
        if (!$find_string)
            return $this->repository->getPrimaryPackagesChunk($offset);

        return $this->repository->getPrimaryPackagesChunkFindByName($offset, $find_string);
    }

    public function secondaryPackageSelector(int $offset = 0, string $find_string = "")
    {
        if (!$find_string)
            return $this->repository->getSecondaryPackagesChunk($offset);

        return $this->repository->getSecondaryPackagesChunkFindByName($offset, $find_string);
    }


}
